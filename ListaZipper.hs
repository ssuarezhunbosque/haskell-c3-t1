module ListaZipper()where

data ListaEncadenadaOrdenada a = Empty | Nodo a (ListaEncadenadaOrdenada a) deriving (Show, Read, Eq)

singleton :: a -> ListaEncadenadaOrdenada a
singleton x = Nodo x Empty

insertarElemento1 :: (Ord a) => a -> ListaEncadenadaOrdenada a ->ListaEncadenadaOrdenada a
insertarElemento1 x Empty = singleton x
insertarElemento1 x (Nodo a lista)
   | x == a = Nodo x lista
   | x > a  = Nodo x (insertarElemento1 a lista)
   | x < a  = Nodo a (insertarElemento1 x lista)

insertarSubLista :: (Ord a) => ListaEncadenadaOrdenada a -> ListaEncadenadaOrdenada a -> ListaEncadenadaOrdenada a
insertarSubLista Empty Empty = Empty
insertarSubLista Empty (Nodo a lista) = (Nodo a lista)
insertarSubLista (Nodo a lista) Empty = (Nodo a lista)
insertarSubLista (Nodo a lista1) (Nodo b lista2) = insertarSubLista lista1 (insertarElemento1 a (Nodo b lista2))

buscarElemento :: (Ord a) => a -> ListaEncadenadaOrdenada a -> Bool  
buscarElemento x Empty = False  
buscarElemento x (Nodo a lista)  
    | x == a = True  
    | x < a  = buscarElemento x lista  
    | x > a  = buscarElemento x lista


data HGCrumb = HGCrumb Nodo [ListaEncadenadaOrdenada] [ListaEncadenadaOrdenada] deriving (Show)  
type HGZipper = (ListaEncadenadaOrdenada, [HGCrumb]) 